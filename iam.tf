

# Policy
resource "aws_iam_policy" "iam_read_only_policy" {
  name        = "iam-read-only-tf"
  path        = "/"
  description = ""
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : [
          "iam:GenerateCredentialReport",
          "iam:GenerateServiceLastAccessedDetails",
          "iam:Get*",
          "iam:List*",
          "iam:SimulateCustomPolicy",
          "iam:SimulatePrincipalPolicy"
        ],
        "Resource" : "*"
      }
    ]
  })
}

# Role
resource "aws_iam_role" "iam_read_only_role" {
  name = "iam-read-onlyinstance-role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Principal = {
          Service = "ec2.amazonaws.com"
        },
        Action = "sts:AssumeRole"
      }
    ]
  })
}

# Role Policy Attachment
resource "aws_iam_policy_attachment" "ec2_instance_policy_attachment" {
  name       = "iam-read-only-policy-attachment"
  roles      = [aws_iam_role.iam_read_only_role.name]
  policy_arn = aws_iam_policy.iam_read_only_policy.arn
}

# Instance Profile
resource "aws_iam_instance_profile" "ec2_instance_profile_for_iam" {
  name = "iam-read-only-instance-profile"
  role = aws_iam_role.iam_read_only_role.name
}