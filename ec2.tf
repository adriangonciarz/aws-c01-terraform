data "aws_ami" "latest_ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*-x86_64-gp2"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_instance" "small_server" {
  ami           = data.aws_ami.latest_ubuntu.id
  instance_type = "t2.micro"
  key_name      = "sockshopk8s"

  tags = {
    Certificate = "C01"
  }

  vpc_security_group_ids      = [aws_security_group.basic_security_group.id]
  associate_public_ip_address = true
  iam_instance_profile        = aws_iam_instance_profile.ec2_instance_profile_for_iam.name
}

output "small_server_arn" {
  value       = aws_instance.small_server.arn
  sensitive   = false
  description = "ARN of created instance"
}

output "small_server_public_dns" {
  value       = aws_instance.small_server.public_dns
  sensitive   = false
  description = "Public DNS of created instance"
}

# Attached EBS Volume
resource "aws_ebs_volume" "tiny_ebs_volume" {
  availability_zone = aws_instance.small_server.availability_zone
  size              = 1

  tags = {
    Certificate = "C01"
  }
}
resource "aws_volume_attachment" "small_instance_attachment" {
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.tiny_ebs_volume.id
  instance_id = aws_instance.small_server.id
}